FROM node:8

RUN mkdir /code
WORKDIR /code

# Yarn
RUN apt-get update && \
    apt-get install apt-transport-https && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && \
    apt-get install yarn

# Install Node deps
COPY package.json /code/package.json
COPY yarn.lock /code/yarn.lock
RUN yarn

VOLUME /code/node_modules
ENV PATH /code/node_modules/.bin:$PATH
