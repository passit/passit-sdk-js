var jasmineWebpackPlugin = require('jasmine-webpack-plugin');

module.exports = {
  entry: './index.js',
  output: {
    filename: 'bundle.js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension. 
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
  },
  devtool: 'source-map',
  node: {
    fs: "empty" // Ignore ts error about fs in libsodium.js
  },
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader` 
      { 
        test: /\.tsx?$/,
        loader: 'ts-loader'
      }
    ]
  },
  plugins: [new jasmineWebpackPlugin({filename: "index.html"})]
}
