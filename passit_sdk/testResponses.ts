/* tslint:disable:max-line-length */
/* tslint:disable:object-literal-key-quotes */
/* tslint:disable:whitespace */
/* tslint:disable:semicolon */
export const clientEmail = "aa@aa.aa";
const clientSalt = "IeCnYeq7lYTBKwp7SN6NBQ==";
const clientPublicKey = "Koc+zt18UoasJYUoXicZ7aDk5MFvyKrblNMaIS9HCWc=";
const clientPrivateKey = "OThMBdLBK2vLG7wgUo3zlJgCyFikr4SGNCiodZaG0LxQart/4IrTGQOvxyenUKafiA8CAjc2DD7pfnzhoaDfmbaTiiUJjgR8dM3NCiNe5JKiGZX6i4ERIw==";
const clientToken = "c32e490b4d4f04743460c39eb2ca4fd52488e545491d8e8df0b4b22fc027445c";

const groupPublicKey = "QyoRhgnvQd1JFogQbZxZQMnzS4isD8LS8WB9KWkuMlg=";
const groupAESKey = "9fA0skHNFOuzAkl+pAtax4Q9e7hR7hPHiiZy5hcePCR9a/dy2PpfbRKpJaVPjrV3BLpxbsrJhx+9CJ2R8vnbtlIvK88dP/6hWv6DTCMy7Opp5OU/AserEvYEfaU=";
const groupPrivateKey = "FE2/UZIKZru8ictqqdfcm0LQP9S5+pm/KT3wcwYvR4kyhYvTjz0Th19mjuQvitwmKvCpqtDtTcwUVQaeaiJzHP4/RPxOQAbZh9luRJGa5yY1MtdV";

export const signUpResp = {
    "id": 1,
    "email": clientEmail,
    "public_key": clientPublicKey,
    "private_key": clientPrivateKey,
    "client_salt": clientSalt,
};
export const publicUser = {
    "client_salt": clientSalt,
};
export const loginResp = {
    "user": {
        "id": 1,
        "email": clientEmail,
        "public_key": clientPublicKey,
        "private_key": clientPrivateKey,
        "client_salt": clientSalt,
    },
    "token": clientToken,
};
export const groupDetailResp = {
    "id": 1,
    "name": "mygroup",
    "groupuser_set": [
        {
            "id": 1,
            "user": 1,
            "group": 1,
            "is_group_admin": true,
        },
    ],
    "public_key": groupPublicKey,
    "my_key_ciphertext": groupAESKey,
    "my_private_key_ciphertext": groupPrivateKey,
};
export const userPublicKeyResp = {
    "id": 2,
    "public_key": "QWle2GHFo88DrAoA6lYmFw8549mMrgbpDDVqEA2HY3g=",
};
export const userAddGroupResp = {
    "user": 2,
    "is_group_admin": false,
    "group": 1,
    "id": 2,
};
export const secretThroughObj = {
    "id": 8,
    "group": null,
    "key_ciphertext": "Eu8on0l1q6vrYyzmaoPfzcgrsijDawOgMrbLvzbUVFIWYjDS898leS1w0+ErJ4Ri0LSSJoPYEqJpLc0Cb9ILvAF8HjGXoju9YAsvfOYHzwfNieH6yxP1kxeR2xI=",
    "data": {
        "password": "FKgFsi44NmMFU2l6wR0+QRtlEha0CwhOILX1oQe2L5edZpPAxKy2dCDoZWp08OY=",
    },
    "public_key": clientPublicKey,
    "is_mine": true,
};
export const secretObj = {
    "id": 9,
    "name": "my secret",
    "type": "website",
    "data": {"username": "myself"},
    "secret_through_set": [secretThroughObj],
};
export const groupSecretThroughObj = {
    "id": 9,
    "group": 1,
    "key_ciphertext": "",
    "data": {
        "password": "",
    },
    "public_key": groupPublicKey,
    "is_mine": true,
};
export const groupSecretObj = {
    "id": 10,
    "name": "my group secret",
    "type": "website",
    "data": {"username": "myself"},
    "secret_through_set": [groupSecretThroughObj],
};
export const createSecretResp = secretObj;
export let updateSecretResp: any = {};
updateSecretResp = Object.assign(updateSecretResp, secretObj);
updateSecretResp.name = "blarg";
export const getSecretResp = secretObj;
export const listSecretResp = [secretObj];

export const groupResp = {
    "id": 1,
    "name": "devs",
    "groupuser_set": [{"id": 7, "user": 1, "group": 1, "is_group_admin": true}],
    "public_key": "7A1ijT2S83er1A/vDHGd0pEloQtcRiMyZ9+P1MPCyWI=",
    "my_key_ciphertext": "e060zKeVF5CvSeR0tmNspnK/l/Yp6WiiYuG+g3u0SGcI93tS/v987EtoeI/to2dCINjIFXzHM0cFyaMrxDAw8d4ftx8UJcvQ3ygYc/NS0m0KzwB7bLv664irPmM=",
    "my_private_key_ciphertext": "WC3b++WgZLwZlOI/jDLmF4GnPzKSzZm7Sm7rJIgpFjPbmBqvM2OVuO2maVzbpgBTbrkTZRokPACOGfId9JvpEetEfn92JjEr0dIhqXIEHqz3ciY/",
};

export const groupsPostResp = {id: 51, name: "My Group", slug: "my-group"}
export const groupGetResp = {"id":51,"name":"My Group","groupuser_set":[{"id":51,"user":1,"group":51,"is_group_admin":true}],"public_key":"7A1ijT2S83er1A/vDHGd0pEloQtcRiMyZ9+P1MPCyWI=","my_key_ciphertext":"e060zKeVF5CvSeR0tmNspnK/l/Yp6WiiYuG+g3u0SGcI93tS/v987EtoeI/to2dCINjIFXzHM0cFyaMrxDAw8d4ftx8UJcvQ3ygYc/NS0m0KzwB7bLv664irPmM=","my_private_key_ciphertext":"WC3b++WgZLwZlOI/jDLmF4GnPzKSzZm7Sm7rJIgpFjPbmBqvM2OVuO2maVzbpgBTbrkTZRokPACOGfId9JvpEetEfn92JjEr0dIhqXIEHqz3ciY/"}
