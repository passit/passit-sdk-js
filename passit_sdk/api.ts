import {
  IChangePassword,
  IConf,
  IContact,
  ICreateGroup,
  ICreateGroupResp,
  ICreateGroupUser,
  ICreateSecret,
  ICreateSecretThroughGroup,
  ICreateUser,
  IGroup,
  IGroupUser,
  ILoginResonse,
  IPublicAuthResponse,
  ISecret,
  ISecretThroughGroup,
  IUpdateSecret,
  IUpdateSecretWithThroughs,
  IUser,
  IUserPublicKey,
} from "./api.interfaces";

export interface IHeaders {
  [propName: string]: string;
}
export default class Api {

  public token: string;

  constructor(public baseUrl = "") { }

  /**
   * Send a login request
   * @param {string} base - the base64 encoding of username and hash of pw
   * @return {Promise<ILoginResonse>} the response in a promise
   */
  public login(base: string, expires?: number): Promise<ILoginResonse> {
    const headers = {
      Authorization: "Basic " + base,
    };
    const data: {expires?: number} = {};
    if (expires) {
      data.expires = expires;
    }
    return this.post("auth/login/", data, headers);
  }

  /** Log out current user, only their current auth token */
  public logout(): Promise<void> {
    return this.post("auth/logout/");
  }

  /** Log out current user on all devices */
  public logoutAll(): Promise<void> {
    return this.post("auth/logoutall/");
  }

  public usernameAvailable(username: string): Promise<{ available: boolean }> {
    return this.get(`username-available/${username}/`);
  }

  public lookupUserID(email: string): Promise<{id: number}> {
    return this.post("lookup-user-id/", {email});
  }

  public createUser(user: ICreateUser): Promise<IUser> {
    return this.post("users/", user);
  }

  public updateUser(id, user: Partial<ICreateUser>): Promise<IUser> {
    return this.patch(`users/${id}/`, user);
  }

  public async getUser(): Promise<IUser> {
    const users = await this.get("users/");
    return users[0];
  }

  public deleteUser(userId: number, password: string) {
    return this.delete(`users/${userId}/`, {password});
  }

  public getPublicAuth(username: string): Promise<IPublicAuthResponse> {
    return this.get(`user-public-auth/${username}/`);
  }

  public getUserPublicKey(id: number): Promise<IUserPublicKey> {
    return this.get(`user-public-key/${id}/`);
  }

  public createGroup(group: ICreateGroup): Promise<ICreateGroupResp> {
    return this.post("groups/", group);
  }

  public updateGroup(id: number, group: Partial<ICreateGroup>): Promise<ICreateGroupResp> {
    return this.patch(`groups/${id}/`, group);
  }

  public deleteGroup(id: number): Promise<void> {
    return this.delete(`groups/${id}/`);
  }

  public getGroup(id: number): Promise<IGroup> {
    return this.get(`groups/${id}/`);
  }

  public getGroups(): Promise<IGroup[]> {
    return this.get("groups/");
  }

  public createGroupUser(groupID: number, groupUser: ICreateGroupUser): Promise<IGroupUser> {
    return this.post(`groups/${groupID}/users/`, groupUser);
  }

  public deleteGroupUser(groupID: number, id: number): Promise<void> {
    return this.delete(`groups/${groupID}/users/${id}/`);
  }

  public acceptGroupInvite(groupUserId: number) {
    return this.post("accept-group-invite/", {id: groupUserId});
  }

  public declineGroupInvite(groupUserId: number) {
    return this.post("reject-group-invite/", {id: groupUserId});
  }

  public getContacts(): Promise<IContact[]> {
    return this.get(`contacts/`);
  }

  public createSecret(secret: ICreateSecret): Promise<ISecret> {
    return this.post("secrets/", secret);
  }

  public updateSecret(id: number, secret: IUpdateSecret): Promise<ISecret> {
    return this.patch(`secrets/${id}/`, secret);
  }

  public updateSecretWithThroughs(id: number, secret: IUpdateSecretWithThroughs): Promise<ISecret> {
    return this.put(`secrets/${id}/`, secret);
  }

  public getSecret(id: number): Promise<ISecret> {
    return this.get(`secrets/${id}/`);
  }

  public getSecrets(): Promise<ISecret[]> {
    return this.get("secrets/");
  }

  public deleteSecret(id: number): Promise<void> {
    return this.delete(`secrets/${id}/`);
  }

  public createSecretGroup(secretID: number, secretGroup: ICreateSecretThroughGroup): Promise<ISecretThroughGroup> {
    return this.post(`secrets/${secretID}/groups/`, secretGroup);
  }

  public deleteSecretGroup(secretID: number, secretThroughID: number): Promise<void> {
    return this.delete(`secrets/${secretID}/groups/${secretThroughID}/`);
  }

  public changePassword(changePassword: IChangePassword): Promise<{ success: boolean }> {
    return this.post("change-password/", changePassword);
  }

  public confirmEmailLongCode(code: string): Promise<{user_id: number}> {
    return this.put("confirm-email-long-code/", {long_code: code});
  }

  public confirmEmailShortCode(code: string): Promise<{user_id: number}> {
    return this.put("confirm-email-short-code/", {short_code: code});
  }

  public requestNewConfirmation(): Promise<void> {
    return this.get("request-new-confirmation/");
  }

  public getConf(): Promise<IConf> {
    return this.get("conf/");
  }

  /* can be overriden in subclass to use another function for dispataching
   * http requests
   */
  protected async sendInner<B>(
    method: string,
    url: string,
    body: B,
    headers: IHeaders,
    returnsJSON: boolean,
  ): Promise<any> {
    const resp = await fetch(url, { body: JSON.stringify(body), headers, method });
    if (resp.status < 200 || resp.status >= 300) {
      throw await resp.json();
    }
    if (returnsJSON) {
      return await resp.json();
    }
    return undefined;
  }
  private get<R>(url: string): Promise<R> {
    return this.send<undefined>("GET", url);
  }

  private post<B, R>(url: string, body?: B, headers?: IHeaders): Promise<R> {
    return this.send("POST", url, { body, headers });
  }

  private patch<B, R>(url: string, body: B): Promise<R> {
    return this.send("PATCH", url, { body });
  }

  private put<B, R>(url: string, body: B): Promise<R> {
    return this.send("PUT", url, { body });
  }

  private delete<B>(url: string, body?: B): Promise<void> {
    const config: any = {returnsJSON: false};
    if (body) {
      config.body = body;
    }
    return this.send<undefined>("DELETE", url, config);
  }

  private send<B>(
    method: string,
    url: string,
    { body, returnsJSON = true, headers }: { body?: B, returnsJSON?: boolean, headers?: IHeaders } = {},
  ): Promise<any> {
    const defaultHeaders: IHeaders = {};
    if (returnsJSON) {
      defaultHeaders.Accept = "application/json";
    }
    if (body) {
      defaultHeaders["Content-Type"] = "application/json";
    }
    if (this.token) {
      defaultHeaders.Authorization = `Token ${this.token}`;
    }
    return this.sendInner<B>(
      method,
      this.baseUrl + url,
      body,
      Object.assign(defaultHeaders, headers),
      returnsJSON,
    );
  }

}
