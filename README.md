Passit.io SDK for javascript

passit-sdk-js allows developers to easily interact with [Passit](http://passit.io) without needing to understand underlying implementation details and cryptography. That said - always use extreme caution when dealing with crypto.

[Documentation](https://passit.gitlab.io/passit-sdk-js/)

# Cryptography

[simple-asymmetric-js](https://gitlab.com/burke-software/simple-asymmetric-js) provides most crpyto functionality. 

In addition passit_sdk/hash.ts contains code needed to hash a password client side. 
For this it uses SubtleCrypto PBKDF2 with SHA-256. The hash uses a random salt and 24000 iterations. 

# Development

Jasmine tests can be executed with docker-compose

1. `docker-compose up`
2. Go to localhost:8080

## Interesting files

passit_sdk/api.ts - handles all http requests.
passit_sdk/sdk.ts - bulk of the program.
passit_sdk/sdk.spec.ts - Jasmine tests
